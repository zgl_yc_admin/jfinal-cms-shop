package com.gz.common;

import com.jfinal.core.JFinal;

/**
 * Created by gongzhen on 2018/6/6.
 */
public class Constant {
    public final static String HOST_PATH = "http://localhost";//主机地址可以为域名
    //public final static String HOST_PATH = "http://gongzhen.site";//主机地址可以为域名
    public static String FILE_PATH=HOST_PATH+"/admin/";//图片文件预览下载地址，'/'表示cms-admin项目根路径，如为admin则此值为HOST_PATH+"/admin/"
    public final static String PROJECT_PATH = JFinal.me().getContextPath()+"/";
}
